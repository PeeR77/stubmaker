import os
import argparse
from pathlib import Path

import CppHeaderParser


INTERSPACE = ' ' * 4


class StubGenerator:
    def __init__(self, params):
        self.params = params
        self.file_or_path = params.file_or_dir
        self.header = None
        self.cpp_str = ''

        if os.path.isdir(self.file_or_path):
            self.create_stubs(self.file_or_path)
        elif os.path.isfile(args.file_or_dir):
            self.create_stub(args.file_or_dir)

    def get_output(self):
        return self.cpp_str

    def save_or_print(self, header_file):
        if self.params.output_path:
            file_name = os.path.basename(header_file)
            cpp_file_path = args.output_path + '/' + os.path.splitext(file_name)[0] + ".cpp"
            StubGenerator.save_file(cpp_file_path, self.cpp_str)
        else:
            print(self.cpp_str)

    def create_stubs(self, path):
        for root, dirs, files in os.walk(path, topdown=False):
            for file in files:
                if file.endswith(".h") or file.endswith(".hpp"):
                    self.create_stub(os.path.join(root, file))

    def create_stub(self, header_file):
        CppHeaderParser.Resolver.NAMESPACES = []  # fix bug of reused previous namespaces
        self.header = CppHeaderParser.CppHeader(header_file)
        self.cpp_str = ''

        self.add_header_include()
        self.add_namespaces()
        self.add_functions_implementations()
        self.add_class_functions_implementations()

        self.save_or_print(header_file)

    @staticmethod
    def save_file(file_path, cpp_str):
        os.makedirs(os.path.dirname(file_path), exist_ok=True)
        with open(file_path, "w") as file:
            file.write(cpp_str)

    @staticmethod
    def get_parameters_str(parameters, comment_parameters=False):
        str = ''
        for p in parameters:
            type = p['typedef']
            if type is None:
                type = p['type']

            if comment_parameters:
                str += '{} /*{}*/, '.format(type, p['name'])
            else:
                str += '{} {}, '.format(type, p['name'])
        return str[:-2]

    @staticmethod
    def generate_param_to_mock_function(parameters):
        param_str = ''
        param_counter = 1

        if len(parameters) == 1 and parameters[0]['type'] == 'void':
            return param_str

        for p in parameters:
            param_name = p['name']
            if not param_name:
                param_name = 'p' + str(param_counter)
                param_counter += 1
            param_str += '{}, '.format(param_name)
        return param_str[:-2]

    @staticmethod
    def get_dummy_return_str(method):
        str = ''
        if method['returns'] == 'void':
            return str
        elif method['returns_pointer']:
            return INTERSPACE + 'return nullptr;\n'

        static_str = ''
        if method['returns_reference']:
            static_str = 'static '

        str += INTERSPACE + '{}{} dummy{{}};\n'.format(static_str, method['returns'])
        str += INTERSPACE + 'return {}dummy;\n'.format('&' if method['returns_pointer'] else '')
        return str

    @staticmethod
    def generate_mock(mock_class_name, method):
        mock_call = INTERSPACE
        if method['returns'] != 'void':
            mock_call += 'return '
        function_parameters_pass = StubGenerator.generate_param_to_mock_function(method['parameters'])
        mock_call += '{}::getMock().{}({});\n'.format(mock_class_name, method['name'], function_parameters_pass)
        return mock_call

    def get_function_imp_str(self, method):
        str = ''

        if len(method) == 0:
            return str

        for namespace in method['namespace'].split('::'):
            if namespace:
                str += 'namespace {} {{\n'.format(namespace)

        parameters = StubGenerator.get_parameters_str(method['parameters'], not self.params.generate_with_mocks)
        if self.params.generate_with_mocks:
            mock_name = 'Mock' + os.path.basename(os.path.splitext(self.header.headerFileName)[0])
            function_content = StubGenerator.generate_mock(mock_name, method)
        else:
            function_content = StubGenerator.get_dummy_return_str(method)

        str += '{} {}({})\n{{\n{}}}\n'.format(method['rtnType'], method['name'], parameters, function_content)

        for namespace in method['namespace'].split('::'):
            if namespace:
                str += '}} // namespace {}\n'.format(namespace)

        str += '\n'
        return str

    def get_class_function_imp_str(self, method):
        class_name = method['parent']['name']
        parameters = StubGenerator.get_parameters_str(method['parameters'], not self.params.generate_with_mocks)
        const_str = ' const' if method['const'] else ''
        return '{} {}::{}({}){}\n{{\n{}}}\n\n'.format(method['rtnType'], class_name, method['name'], parameters,
                                                      const_str, StubGenerator.get_dummy_return_str(method))

    @staticmethod
    def get_constructor_imp_str(method):
        assert method['constructor']
        parameters = StubGenerator.get_parameters_str(method['parameters'])
        return '{}::{}({})\n{{\n}}\n\n'.format(method['name'], method['name'], parameters)

    @staticmethod
    def get_destructor_imp_str(method):
        assert method['destructor']
        parameters = StubGenerator.get_parameters_str(method['parameters'])
        return '{}::~{}({})\n{{\n}}\n\n'.format(method['name'], method['name'], parameters)

    def add_header_include(self):
        self.cpp_str += '#include "{}"\n\n'.format(Path(self.header.headerFileName).name)
        self.cpp_str += '\n'

    def add_namespaces(self):
        for namespace in self.header.namespaces:
            self.cpp_str += 'using namespace {};\n'.format(namespace)
        if len(self.header.namespaces) > 0:
            self.cpp_str += '\n\n'

    def add_functions_implementations(self):
        functions = self.header.functions
        for function in functions:
            self.cpp_str += self.get_function_imp_str(function)

    def add_class_functions_implementations(self):
        class_list = self.header.classes_order
        for current_class in class_list:
            methods = self.get_class_functions(current_class)
            for method in methods:
                if not method['defined'] and not method['pure_virtual']:
                    if method['constructor']:
                        self.cpp_str += self.get_constructor_imp_str(method)
                    elif method['destructor']:
                        self.cpp_str += self.get_destructor_imp_str(method)
                    else:
                        self.cpp_str += self.get_class_function_imp_str(method)

    def get_class_functions(self, current_class):
        methods = current_class['methods']['public']

        if self.params.all_class_functions:
            methods += current_class['methods']['protected'] + current_class['methods']['private']
        else:
            # Add protected/private constructors/destructors
            for method in current_class['methods']['protected'] + current_class['methods']['private']:
                if method['constructor'] or method['destructor']:
                    methods.append(method)

        return methods


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='StubMaker - App will generate functions stabs from header files for C++ project.')
    parser.add_argument('file_or_dir', help='C++ header file or directory with headers.')
    parser.add_argument('-o', dest='output_path', required=False,
                        help='C++ implementation file path to save. Otherwise, will just print the stubs.')
    parser.add_argument('-a', dest='all_class_functions', action='store_true',
                        help='Generate also class protected and private functions.')
    parser.add_argument('-m', dest='generate_with_mocks', action='store_true',
                        help='Generate stub functions with gmock calls.')

    args = parser.parse_args()

    stub_generator = StubGenerator(args)
