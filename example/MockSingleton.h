#pragma once

#include <memory>


template <typename T>
class MockSingleton
{
public:
    static T& getMock()
    {
        if(!m_instance)
        {
            m_instance.reset(new T());
        }
        return *m_instance.get();
    }

    static void resetMock()
    {
        m_instance.reset();
    }

private:
    static std::unique_ptr<T> m_instance;
};

template <typename T>
std::unique_ptr<T> MockSingleton<T>::m_instance;
