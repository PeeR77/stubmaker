#include <string>

int function_header2();

namespace my2_namespace1
{

int function1();

namespace my2_namespace2
{

int function2(int a);

class TestClass2
{
    TestClass2(int a, const char* name);

public:
    TestClass2() {};
    TestClass2(int a);
    ~TestClass2();

public:
    void implementedFunction(const std::string& str, int p1, float p2) {}
    void functionVoid(const std::string& str, int p1, float p2);
    int functionInt(const std::string& str, int p1, float p2);
    std::string functionStr(const std::string& str, int p1, float p2);
    int& functionIntRef(const std::string& str, int& p1, float* p2);
    int* functionIntPointer(const std::string& str, int& p1, float* p2);

protected:
    void implementedFunction2(const std::string& str, int p1, float p2) {}
    void functionVoid2(const std::string& str, int p1, float p2);
    int functionInt2(const std::string& str, int p1, float p2);
    int& functionIntRef2(const std::string& str, int& p1, float* p2);
    int* functionIntPointer2(const std::string& str, int& p1, float* p2);

private:
    void implementedFunction3(const std::string& str, int p1, float p2) {}
    void functionVoid3(const std::string& str, int p1, float p2);
    int functionInt3(const std::string& str, int p1, float p2);
    int& functionIntRef3(const std::string& str, int& p1, float* p2);
    int* functionIntPointer3(const std::string& str, int& p1, float* p2);
};

} // namespace my2_namespace2
} // namespace my2_namespace1
