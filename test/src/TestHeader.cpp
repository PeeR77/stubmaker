#include "TestHeader.hpp"


using namespace my_namespace1;
using namespace my_namespace1::my_namespace2;


int function0()
{
    int dummy{};
    return dummy;
}

namespace my_namespace1 {
int function1()
{
    int dummy{};
    return dummy;
}
} // namespace my_namespace1

namespace my_namespace1 {
namespace my_namespace2 {
int function2(int /*a*/)
{
    int dummy{};
    return dummy;
}
} // namespace my_namespace1
} // namespace my_namespace2

TestClass::TestClass(int /*a*/)
{
}

TestClass::~TestClass()
{
}

void TestClass::functionVoid(const std::string & /*str*/, int /*p1*/, float /*p2*/)
{
}

void TestClass::functionVoidConst(const std::string & /*str*/, int /*p1*/, float /*p2*/) const
{
}

int TestClass::functionInt(const std::string & /*str*/, int /*p1*/, float /*p2*/)
{
    int dummy{};
    return dummy;
}

std::string TestClass::functionStr(const std::string & /*str*/, int /*p1*/, float /*p2*/)
{
    std::string dummy{};
    return dummy;
}

int & TestClass::functionIntRef(const std::string & /*str*/, int & /*p1*/, float * /*p2*/)
{
    static int dummy{};
    return dummy;
}

int * TestClass::functionIntPointer(const std::string & /*str*/, int & /*p1*/, float * /*p2*/)
{
    static int dummy{};
    return &dummy;
}

TestClass::TestClass(int /*a*/, const char * /*name*/)
{
}

