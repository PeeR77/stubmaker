#include "TestHeader2.hpp"


using namespace my2_namespace1;
using namespace my2_namespace1::my2_namespace2;


int function_header2()
{
    int dummy{};
    return dummy;
}

namespace my2_namespace1 {
int function1()
{
    int dummy{};
    return dummy;
}
} // namespace my2_namespace1

namespace my2_namespace1 {
namespace my2_namespace2 {
int function2(int /*a*/)
{
    int dummy{};
    return dummy;
}
} // namespace my2_namespace1
} // namespace my2_namespace2

TestClass2::TestClass2(int /*a*/)
{
}

TestClass2::~TestClass2()
{
}

void TestClass2::functionVoid(const std::string & /*str*/, int /*p1*/, float /*p2*/)
{
}

int TestClass2::functionInt(const std::string & /*str*/, int /*p1*/, float /*p2*/)
{
    int dummy{};
    return dummy;
}

std::string TestClass2::functionStr(const std::string & /*str*/, int /*p1*/, float /*p2*/)
{
    std::string dummy{};
    return dummy;
}

int & TestClass2::functionIntRef(const std::string & /*str*/, int & /*p1*/, float * /*p2*/)
{
    static int dummy{};
    return dummy;
}

int * TestClass2::functionIntPointer(const std::string & /*str*/, int & /*p1*/, float * /*p2*/)
{
    static int dummy{};
    return &dummy;
}

TestClass2::TestClass2(int /*a*/, const char * /*name*/)
{
}

