#include "TestHeader.hpp"

int main()
{
    using namespace my_namespace1;
    using namespace my_namespace1::my_namespace2;

    function0();
    function1();
    function2(1);

    TestClass test;
    int a = test.functionInt("", 1, 2);
    test.functionVoid("", a, 0.5f);

    int p1{};
    float p2{};
    test.functionIntPointer("abc", p1, &p2);
}
